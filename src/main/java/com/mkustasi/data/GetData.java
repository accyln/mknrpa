package com.mkustasi.data;

public class GetData {

	public static String FILESYSDATE;
	public static final int DEFAULT_WAIT = 20;
	public static final int DEFAULT_WAIT_LOADERBOX = 40;
	public static final String REMOTE_MACHINE_IP = "http://hub:4444/wd/hub";

	public enum Url {
		MAKINEUSTASI_URL;

	}


	public enum Data {

		EMAIL,
		PASSWORD,
		AD,
		SOYAD;

		public String getValue() {

			return DataFinder.getData(this);
		}
	}
}
