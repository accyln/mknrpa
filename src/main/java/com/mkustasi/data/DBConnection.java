package com.mkustasi.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mkustasi.base.SeleniumAbstractTest;
import com.mkustasi.utility.log;



public class DBConnection extends SeleniumAbstractTest {

	

	
	public static List<String[]> resultToTable(ResultSet rs) {

		List<String[]> table = null;
		try {
			table = new ArrayList<>();
			int nCol = rs.getMetaData()
			             .getColumnCount();
			while (rs.next()) {
				String[] row = new String[nCol];
				for (int iCol = 1; iCol <= nCol; iCol++) {
					Object obj = rs.getObject(iCol);
					row[iCol - 1] = (obj == null) ? null : obj.toString();
				}
				table.add(row);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			log.debug("" + e);
		}
		return table;
	}

	public static List<String> getColumnNames(ResultSet rs) {

		List<String> table = null;
		try {
			table = new ArrayList<>();
			ResultSetMetaData rsmd = rs.getMetaData();

			int colCount = rsmd.getColumnCount();

			for (int i = 1; i <= colCount; i++) {
				table.add(rsmd.getColumnLabel(i));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return table;
	}
	


	
	public static List<String[]> selectRpaSQLserver(String sql, String aciklama) {


		String connectionUrl = "connectionUrl";
		Connection conn = null;
		List<String[]> table = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = DriverManager.getConnection(connectionUrl);

			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			List<String> columnNames = getColumnNames(rs);
			table = resultToTable(rs);
		} catch (Exception e) {
			log.info("Database baglatisinda hata alindi" + e);
			
		}

		return table;
	}
}
