package com.mkustasi.commons;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.model.LayoutReport;
import com.mkustasi.basepages.SeleniumAbstractPage;


public class Commons extends SeleniumAbstractPage {

	public TestVariables testVariables;


	public Commons(WebDriver driver) {

		super(driver);
		testVariables = new TestVariables();

		PageFactory.initElements(this.driver, this);
	}

	public class TestVariables {


	}

	public void control(WebElement elem, String onTrue, String onFalse) {

		try {
			if (getTextOfElement(elem).contains(onTrue)) {
				LogPASS(onTrue);
			} else {
				LogFAIL(onFalse);
				Assert.assertTrue(false);
			}
		} catch (Exception e) {
			LogFAIL(onFalse);
			Assert.assertTrue(false);
		}
	}


}
