package com.mkustasi.pages;

import com.google.gson.annotations.SerializedName;

public class Urun {
	 @SerializedName("@context") 
	    public String context;
	    @SerializedName("@type") 
	    public String type;
	    public String name;
	    public String image;
	    public String description;
	    public String sku;
	    public String mpn;
	    public String model;
	    public String category;
	    public Brand brand;
	    public Offers offers;
	    public Review review;
	    public AggregateRating aggregateRating;
	    public IsAccessoryOrSparePartFor isAccessoryOrSparePartFor;
}

 class AggregateRating{
    @SerializedName("@type") 
    public String type;
    public String ratingValue;
    public String ratingCount;
}

 class Author{
    @SerializedName("@type") 
    public String type;
    public String name;
}

 class Brand{
    @SerializedName("@type") 
    public String type;
    public String name;
}

 class IsAccessoryOrSparePartFor{
    public String name;
    public String description;
    public String sku;
    public String mpn;
    public String model;
    public String image;
    public Brand brand;
    public Offers offers;
    public Review review;
    public AggregateRating aggregateRating;
}

 class Offers{
    @SerializedName("@type") 
    public String type;
    public String price;
    public String priceCurrency;
    public String priceValidUntil;
    public String availability;
    public String url;
}

 class Review{
    @SerializedName("@type") 
    public String type;
    public ReviewRating reviewRating;
    public Author author;
}

 class ReviewRating{
    @SerializedName("@type") 
    public String type;
    public String ratingValue;
    public String bestRating;
}
