package com.mkustasi.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mkustasi.commons.Commons;
import com.mkustasi.data.GetData.Data;
import com.mkustasi.data.GetData.Url;

public class PageAnaSayfa {

	public Commons lib;

	public PageAnaSayfa(WebDriver driver) {

		lib = new Commons(driver);
		PageFactory.initElements(driver, this);
	}

	public PageAnaSayfa(Commons lib) {

		this.lib = lib;
		PageFactory.initElements(this.lib.driver, this);
	}

	@FindBy(how = How.XPATH, using = "//li[@class='item col-lg-3 col-md-4 col-sm-6 col-xs-6 post-255 product type-product status-publish has-post-thumbnail product_cat-electronics product_cat-home-appliances product_cat-vacuum-cleaner product_brand-apoteket first instock sale featured shipping-taxable purchasable product-type-simple']")
	List<WebElement> urunler;
	@FindBy(how = How.XPATH, using = "//script[@type='application/ld+json']")
	WebElement content;


	public PageAnaSayfa anaSayfayaGit() {

		lib.navigateTo(Url.MAKINEUSTASI_URL);

		return this;
	}

	public PageLogin UrunEkrani() {


		return new PageLogin(lib);
	}

	public PageAnaSayfa girisYap() {


		return this;
	}
}
