package com.mkustasi.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mkustasi.commons.Commons;
import com.mkustasi.data.GetData.Data;

public class PageLogin {

	Commons lib;

	public PageLogin(Commons lib) {

		this.lib = lib;
		PageFactory.initElements(this.lib.driver, this);
		lib.Control(lib.isElementExist(By.xpath("//*[text()='Üye Girişi']")), "'Üye Girişi' sayfası açıldı.", "'Üye Girişi' sayfası açılamadı!");
	}

	@FindBy(how = How.ID, using = "username")
	WebElement txtEmail;
	@FindBy(how = How.ID, using = "password")
	WebElement txtSifre;
	@FindBy(how = How.ID, using = "userLoginSubmitButton")
	WebElement btnGirisYap;

	public PageAnaSayfa login(Data email, Data password) {

		emailGir(email.getValue()).sifreGir(password.getValue())
		                          .girisYapaTiklaGitAnaSayfaya();

		return new PageAnaSayfa(lib);
	}

	public PageLogin emailGir(String email) {

		lib.sendKeys(txtEmail, email);
		return this;
	}

	public PageLogin sifreGir(String sifre) {

		lib.sendKeys(txtSifre, sifre);
		return this;
	}

	public PageLogin girisYapaTikla() {

		lib.click(btnGirisYap);
		return this;
	}

	public PageAnaSayfa girisYapaTiklaGitAnaSayfaya() {

		lib.click(btnGirisYap);
		return new PageAnaSayfa(lib);
	}
	
	public Mknustasi mknustasi() {


		return new Mknustasi(lib);
	}

}
