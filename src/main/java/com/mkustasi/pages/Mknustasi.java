package com.mkustasi.pages;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mkustasi.commons.Commons;
import com.mkustasi.utility.log;

public class Mknustasi {
	
	public Commons lib;

	public Mknustasi(Commons lib) {

		this.lib = lib;
		PageFactory.initElements(this.lib.driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//li[@class='item col-lg-3 col-md-4 col-sm-6 col-xs-6 post-255 product type-product status-publish has-post-thumbnail product_cat-electronics product_cat-home-appliances product_cat-vacuum-cleaner product_brand-apoteket first instock sale featured shipping-taxable purchasable product-type-simple']")
	List<WebElement> urunler;
	
	@FindBy(how = How.XPATH, using = "//div[@class='item-detail']//div[@class='item-img products-thumb']//a")
	List<WebElement> urunLinkler;
	@FindBy(how = How.XPATH, using = "//script[@type='application/ld+json']")
	WebElement content;

	
	//div[@class='item-detail']//div[@class='item-img products-thumb']//a

	
	
	
	
	public Mknustasi urunDetaylariniAl() {
		
		
		for (int i=0; i<20000 ; i++) {
			
			kwdSayfadakiUrunBilgileriniAl();
			sayfaDeğiştir();
		}
		
		
		return this;
	}
	
	
	public Mknustasi kwdSayfadakiUrunBilgileriniAl() {
		
		for (WebElement urun : urunLinkler) {
			lib.Wait(300);
			urunuYeniTabdaAc(lib.getProperty(urun, "href"));
			lib.Wait(200);
			contentAl();
			tabKapat();
		}
		
		log.info("Sayfadaki ürünler için işlem tamamlandı.");
		
		return this;
	}
	
	
public Mknustasi sayfaDeğiştir() {

        int currentPage=Integer.parseInt(lib.getTextOfElement(By.xpath("//ul[@class='page-numbers']//li/span[@class='page-numbers current']"), 0));
        
        int nextPage=currentPage+1;
		
		lib.click(By.xpath("//ul[@class='page-numbers']//li/a[text()='"+nextPage+"']"));
		
	    log.info("Sayfa değiştirildi"+nextPage);
		lib.Wait(500);

		return this;
	}
	
	
	public Mknustasi uruneTikla() {


		
		lib.click(urunler.get(0));;

		return this;
	}
	
public Mknustasi urunuYeniTabdaAc(String link) {


	lib.driver.switchTo().newWindow(WindowType.TAB);
	
	lib.driver.get(link);

    //lib.driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");

		return this;
	}
	
	
	
	public Mknustasi contentAl() {

		String acc=content.getAttribute("innerHTML");
//		log.info(acc);

			    Urun user = new Gson().fromJson(acc, Urun.class);
			    
			    //String acc

			    System.out.println(user.name);
			    
			    System.out.println(user.brand.name);
			    //DB ENTEGRASYONU
		
		return this;
	}
	
	public Mknustasi geri() {

		lib.driver.navigate().back();
		
		return this;
	}
	
	public Mknustasi tabKapat() {
		
		ArrayList<String> tabs2 = new ArrayList<String> (lib.driver.getWindowHandles());

		lib.driver.switchTo().window(tabs2.get(1));
		lib.driver.close();
		lib.driver.switchTo().window(tabs2.get(0));
		
		return this;
	}
}
