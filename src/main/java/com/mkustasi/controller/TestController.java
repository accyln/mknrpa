package com.mkustasi.controller;

import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;

import com.mkustasi.base.SeleniumAbstractTest;
import com.mkustasi.pages.PageAnaSayfa;
import com.mkustasi.utility.ExtentTestManager;

public class TestController extends SeleniumAbstractTest {

	protected ThreadLocal<PageAnaSayfa> tl = new ThreadLocal<PageAnaSayfa>();

	@BeforeMethod
	public void Before(Method method) {

		ExtentTestManager.startTest(method.getName());
	}

	protected PageAnaSayfa startTest() {

		WebDriver driver = null;
		driver = super.setUpBrowser();
		tl.set(new PageAnaSayfa(driver));

		return tl.get();
	}

}
